import struct
from scapy.all import *

def macDoBajtov(paMac):
    return bytes.fromhex(paMac.replace(":", ""))

def nastavBit(paVstup, paBit):
    return paVstup | (1 << paBit)

class EthRamec():
    def __init__(self, paSrcMac):
        self.aDstMac = "01:00:0c:cc:cc:cc"
        self.aSrcMac = paSrcMac

    def pridajPayload(self, paPayload):
        self.aPayload = paPayload

    def dajBajty(self):
        payloadBajty = self.aPayload.dajBajty()
        return macDoBajtov(self.aDstMac) + macDoBajtov(self.aSrcMac) + struct.pack("!H", len(payloadBajty)) + payloadBajty

class LlcSnap():
    def __init__(self):
        self.aDSAP = 0xaa
        self.aSSAP = 0xaa
        self.aControl = 0x03
        self.aOUI = "00:00:0c"
        self.pProtocolID = 0x2000

    def pridajPayload(self, paPayload):
        self.aPayload = paPayload

    def dajBajty(self):
        return struct.pack("!3B", self.aDSAP, self.aSSAP, self.aControl) + macDoBajtov(self.aOUI) + struct.pack("!H", self.pProtocolID) + self.aPayload.dajBajty()


class Cdp():
    def __init__(self):
        self.aVersion = 1
        self.aTTL = 255
        self.aChecksum = 0
        self.aTLVs = list() 

    def pridajTLV(self, paTLV):
        self.aTLVs.append(paTLV)

    def dajBajty(self):
        bajty = struct.pack("!BBH", self.aVersion, self.aTTL, self.aChecksum)
        for tlv in self.aTLVs:
            bajty += tlv.dajBajty()
            return bajty

cdp = Cdp()



llc = LlcSnap()
llc.pridajPayload(cdp)


ramec = EthRamec("00:23:24:03:55:E4")
ramec.pridajPayload(llc)

#IFACES.show()

rozhranie = IFACES.dev_from_index(23)
sock = conf.L2socket(iface=rozhranie)

sock.send(ramec.dajBajty())